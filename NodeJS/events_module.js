const EventEmitter = require('events')
const emitter = new EventEmitter()

// Register Event
emitter.on('logging', (arg) => {
    console.log(`Message : ${arg.message}`)
})

// Emit Register Event
emitter.emit('logging', {'message': 'logging msg'})

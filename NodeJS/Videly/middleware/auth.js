const jwt = require('jsonwebtoken')
const config = require('config')

function auth(req, res, next){

    // Chekc availability of token
    const token = req.header('x-auth-token')
    if(!token) return res.status(401).send('Access Denied: No Token Provided')

    // Verify token 
    try{
        const verifyToken = jwt.verify(token, config.get('jwtPrivateKey'))
        req.user = verifyToken
        next()
    }
    catch(ex){
        res.status(400).send('Invalid Token')
    }
}

module.exports = auth
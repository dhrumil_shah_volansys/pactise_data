const {Rental} = require('../../models/rental')
const mongoose = require('mongoose');
const request = require('supertest')

describe('/api/returns', () => {

    // Server open and close
    let server;
    let rental;
    let customerId;
    let movieId;
    
    beforeEach(async () => {
        customerId = mongoose.Types.ObjectId()
        movieId = mongoose.Types.ObjectId()
        server = require('../../index');
        rental = new Rental({
            customer:{
                _id: customerId,
                name: '12345',
                phone: '12345'
            },
            movie: {
                _id: movieId,
                title: '12345',
                dailyRentalRate: 2
            }
        })
        await rental.save()
    })
    afterEach(async () => {
        await server.close()
        await Rental.remove() // clear database entry before every test run
    })

    // Tests 
    it('should return 401 if client not logged in', async () => {
        const res = await request(server)
            .post('/api/returns')
            .send({customerId, movieId})
        expect(res.status).toBe(401)      
    })
})
const request = require('supertest')
const {Genre} = require('../../models/genres')
const {User} = require('../../models/user')

let server

describe('/api/genres', () => {

    beforeEach(() => {server = require('../../index')})
    afterEach(async () => {
        await server.close()
        await Genre.remove()
    })

    describe('GET /', () => {
        it('should return all genres', async () => {

            // Genre Creation 
            await Genre.collection.insertMany([
                {name: 'Genre1'},
                {name: 'Genre2'}
            ])

            const res = await request(server).get('/api/genres')
            expect(res.status).toBe(200)
            expect(res.body.length).toBe(2)
            expect(res.body.some(g => g.name === 'Genre1')).toBeTruthy()
            expect(res.body.some(g => g.name === 'Genre2')).toBeTruthy()
        })
    })

    describe('GET /:id', () => {
        it('should return genre for valid id', async () => {

            // Genre Creation 
            const genre = new Genre({name: 'Genre1'})
            await genre.save()

            const res = await request(server).get('/api/genres/' + genre._id)
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('name', genre.name)
        })
        
        it('should return 404 if invalid id passed', async () => {

            const res = await request(server).get('/api/genres/1')
            expect(res.status).toBe(404)
        })
    })

    describe('POST /', () => {
        it('should return 401 if client not logged in', async () => {
            const res = await request(server)
                .post('/api/genres')
                .send({name: 'Genre1'})
            expect(res.status).toBe(401)
        })

        it('should return 400 if gener is less than 5 char', async () => {
            const token = new User().generateUserToken()

            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({name: 'Genr'})
            expect(res.status).toBe(400)
        })

        it('should return 400 if gener is greater than 255 char', async () => {
            const token = new User().generateUserToken()

            const name = new Array(257).join('a')

            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({name: name})
            expect(res.status).toBe(400)
        })

        it('should return genre if it is valid', async () => {
            const token = new User().generateUserToken()

            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({name: 'genre1'})
            
            const genre = await Genre.find({name: 'genre1'})

            expect(genre).not.toBeNull()
        })

        it('should return genre if it is valid', async () => {
            const token = new User().generateUserToken()

            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({name: 'genre1'})

            expect(res.body).toHaveProperty('_id')
            expect(res.body).toHaveProperty('name', 'genre1')
        })
    })
})
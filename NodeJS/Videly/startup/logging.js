require('express-async-errors')
const winston = require('winston')
// require('winston-mongodb')

module.exports = function(){

    // Handle uncaughtException 
    process.on('uncaughtException', (ex) => {
        winston.error(ex.message, ex)
    })

    // Handle unhandledRejection 
    process.on('unhandledRejection', (ex) => {
        winston.error(ex.message, ex)
    })
    
    // Logging Errors 
    winston.add(new winston.transports.File({filename: 'logfile.log'}))
    // winston.add(new winston.transports.MongoDB({db: 'mongodb://localhost/videly'}))
}
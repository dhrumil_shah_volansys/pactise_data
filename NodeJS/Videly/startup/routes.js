const express = require('express')
const genres = require('../routes/genres')
const customer = require('../routes/customer')
const movies = require('../routes/movies')
const rental = require('../routes/rental')
const user = require('../routes/user')
const auth = require('../routes/auth')
const error = require('../middleware/error')
const returns = require('../routes/returns')

module.exports = function(app){

    // Middleware
    app.use(express.json())
    app.use('/api/genres', genres)
    app.use('/api/customer', customer)
    app.use('/api/movies', movies)
    app.use('/api/rental', rental)
    app.use('/api/user', user)
    app.use('/api/auth', auth)
    app.use('/api/returns', returns)
    app.use(error)
}
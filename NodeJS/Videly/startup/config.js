const config = require('config')

module.exports = function(){
    
    // JwtPrivatekey handler 
    if (!config.get("jwtPrivateKey")){
        throw new Error("FATAL error: jwtPrivateKey is not defined")
    }
}
const {genreSchema} = require('../models/genres')
const mongoose = require('mongoose')
const Joi = require("joi")

// Schema Creation 
const movieSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    },
    genre: {
        type: genreSchema,
        required: true  
    },
    numberInStock: {
        type: Number,
        required: true,
        min: 3,
        max: 5
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 3,
        max: 5
    }
})

// Table or Model Creation 
const Movie = mongoose.model('Movie', movieSchema)

// Validate Body Data 
function validateBodyData(movie){
    const schema = Joi.object({
        title: Joi.string().min(3).required(),
        genre: Joi.string().required(),
        numberInStock: Joi.number().min(0).required(),
        dailyRentalRate: Joi.number().min(0).required()
    })
    schema.validate(movie)
}

exports.Movie = Movie
exports.validateBodyData = validateBodyData
const Joi = require('joi')
const mongoose = require('mongoose')

// Schema Creation 
const rentalSchema = new mongoose.Schema({
    customer: {
        type: new mongoose.Schema({
            isGold: {
                type: Boolean,
                default: false
            },
            name: {
                type: String,
                required: true,
                minlength: 5,
                maxlength: 255
            },
            phone: {
                type: String,
                required: true,
                minlength: 5,
                maxlength: 12
            }
        }),
        required: true
    },
    movie: {
        type: new mongoose.Schema({
            title: {
                type: String,
                required: true,
                minlength: 5,
                maxlength: 255
            },
            dailyRentalRate: {
                type: Number,
                required: true,
                min: 0,
                max: 255
            }
        }),
        required: true
    },
    dateOut: {
        type: Date,
        required: true,
        default: Date.now
    },
    dateReturned: {
        type: Date,
    },
    rentalFee: {
        type: Number,
        min: 0
    }
})

// Table or Model Creation 
const Rental = mongoose.model('Rental', rentalSchema)

function validateBodyData(rental){
    const schema = Joi.object({
        customerId: Joi.string().required,
        movieId: Joi.string().required
    })
    return schema.validate(rental)
}

exports.Rental = Rental
exports.validateBodyData = validateBodyData
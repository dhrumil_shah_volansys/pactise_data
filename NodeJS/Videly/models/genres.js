const mongoose = require('mongoose')
const Joi = require("joi")

// Schema Creation
const genreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
    }
})

// Table Creation 
const Genre = mongoose.model('Genre', genreSchema)

// Requested Body Data Validation
function validateBodyData(genre){
    const schema = Joi.object({
        name: Joi.string().min(5).max(255).required()
    })
    return schema.validate(genre)
}

exports.Genre = Genre
exports.validateBodyData = validateBodyData
exports.genreSchema = genreSchema
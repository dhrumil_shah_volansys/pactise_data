const mongoose = require('mongoose')
const Joi = require("joi")

// Schema Creation 
const customerSchema = new mongoose.Schema({
    isGold: {
        type: Boolean
    },
    name: {
        type: String
    },
    phone: {
        type: String
    }
})

// Table or Model Creation 
const Customers = mongoose.model('Customers', customerSchema)

// Requested Body Data Validation
function validateBodyData(customers){
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        phone: Joi.string().min(3).required(),
        isGold: Joi.boolean()
    })
    return schema.validate(customers)
}

exports.Customers = Customers
exports.validate = validateBodyData

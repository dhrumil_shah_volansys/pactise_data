const {Rental, validateBodyData} = require('../models/rental')
const {Customers} = require('../models/customer')
const {Movie} = require('../models/movies')
const express = require("express")
const fawn = require('fawn')
const router = express.Router()

// get all customers
router.get('/', async (req, res) => {
    const rental = await Rental.find().sort('-dateOut')
    res.send(rental)
})

// get particular customers
router.get('/:id', async (req, res) => {

    // get customers 
    const rental = await Rental.findById(
        req.params.id
    )

    // check the customers id is existing or not
    if(!rental){
        res.status(400).send("ID Not Found")
        return
    }

    res.send(rental)
})

// create new customers
router.post('/', async (req, res) => {

    // validate body data
    const {error} = validateBodyData(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    const customer = await Customers.findById(req.body.customerId)
    if (!customer) return res.status(400).send('Customer Not Found...')

    const movie = await Movie.findById(req.body.movieId)
    if (!movie) return res.status(400).send('Movie Not Found...')

    // create customers
    let rental = new Rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone
        },
        movie: {
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate
        }
    })
    try{
        new fawn.Task()
            .save('rentals', rental)
            .update('movies', {_id: movie._id}, {
                $inc: {numberInStock: -1}
            })
            .run()
        res.send(rental)
    }   
    catch(ex){
        res.status(500).send("Something went wrong.")
    }
})

module.exports = router;
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')
const {User} = require('../models/user')
const express = require('express')
const router = express.Router()

router.post('/', async (req, res) => {

    // Validate Body Data 
    const {error} = validateAuthUser(req.body)
    if (error) return res.status(400).send(error.details[0].message)

    // Validate Email existance
    const authUser = await User.findOne({email: req.body.email})
    if (!authUser) return res.status(400).send("Email Does not Exists...")

    // Validate Password 
    const authUserPassword = await bcrypt.compare(req.body.password, authUser.password)
    if (!authUserPassword) return res.status(400).send("Invalid Password...")
    
    // Call generateUserToken function
    const token = authUser.generateUserToken()
    res.send(token)
})

function validateAuthUser(authUser){
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required()
    })
    return schema.validate(authUser)
}

module.exports = router
const bcrypt = require("bcrypt")
const _ = require("lodash")
const auth = require('../middleware/auth')
const {User, validateUser} = require('../models/user')
const express = require('express')
const router = express.Router()

router.get('/me', auth, async (req, res) => {
    const tokenUser = await User.findById(req.user._id).select('-password')
    res.send(tokenUser)
})

// New User Creation 
router.post('/', async (req, res) => {

    // Validate User Data 
    const {error} = validateUser(req.body)
    if (error) return res.status(400).send(error.details[0].message)

    // Validate Unique Email 
    let emailExists = await User.findOne({email: req.body.email})
    if (emailExists) return res.status(400).send("Email already in used...")

    // Create User 
    const user = new User(_.pick(req.body, ['name', 'email', 'password']))

    // Hash password Creation 
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password, salt)

    // Save Created User 
    await user.save()

    // Send responce token in header
    const token = user.generateUserToken()
    res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']))
})

module.exports = router
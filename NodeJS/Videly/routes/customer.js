const {Customers, validate} = require('../models/customer')
const express = require("express")
const router = express.Router()

// get all customers
router.get('/', async (req, res) => {
    const customers = await Customers.find().sort('name')
    res.send(customers)
})


// get particular customers
router.get('/:id', async (req, res) => {

    // get customers 
    const customers = await Customers.findById(
        req.params.id
    )

    // check the customers id is existing or not
    if(!customers){
        res.status(400).send("ID Not Found")
        return
    }

    res.send(customers)
})

// create new customers
router.post('/', async (req, res) => {

    // validate body data
    const {error} = validate(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    // create customers
    let customers = new Customers({
        name: req.body.name,
        phone: req.body.phone,
        isGold: req.body.isGold
    })
    customers = await customers.save()
    res.send(customers)
})

// edit existing customers
router.put('/:id', async (req, res) => {

    // validate body data
    const {error} = validate(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    // update data
    const customers = await Customers.findByIdAndUpdate(
        req.params.id,
        {
            isGold: req.body.isGold,
            name: req.body.name,
            phone: req.body.phone,
            new: true
        }       
    )

    // check the customers id is existing or not
    if(!customers){
        res.status(400).send("ID not found")
        return
    }

    res.send(customers)
})

// delete customers
router.delete('/:id', async (req, res) => {

    // delete customers
    const customers = await Customers.findByIdAndRemove(
        req.params.id
    )

    // check customers id is exists or not
    if(!customers){
        res.status(404).send("ID not found")
        return
    }

    res.send(customers)
})

module.exports = router;
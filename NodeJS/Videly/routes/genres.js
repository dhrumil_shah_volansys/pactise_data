const {Genre, validateBodyData} = require('../models/genres')
const auth = require('../middleware/auth')
const admin = require('../middleware/admin')
const validateObjectId = require('../middleware/validateObjectId')
const mongoose = require('mongoose')
const express = require("express")
const router = express.Router()

// get all genres
router.get('/', async (req, res) => {
    const genres = await Genre.find().sort('name')
    res.send(genres)
})

// get particular genres
router.get('/:id', validateObjectId, async (req, res) => {

    // get genres 
    const genres = await Genre.findById(
        req.params.id
    )

    // check the genre id is existing or not
    if(!genres){
        res.status(400).send("ID Not Found")
        return
    }

    res.send(genres)
})

// create new genres
router.post('/', auth, async (req, res) => {

    // validate body data
    const {error} = validateBodyData(req.body)
    if(error){
        res.status(400).send(error.details[0].message)
        return
    }

    // create genres
    let genres = new Genre({
        name: req.body.name
    })
    genres = await genres.save()
    res.send(genres)
})

// edit existing genre
router.put('/:id', async (req, res) => {

    // validate body data
    const {error} = validateBodyData(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    // update data
    const genres = await Genre.findByIdAndUpdate(
        req.params.id,
        {
            name: req.body.name,
            new: true
        }       
    )

    // check the genre id is existing or not
    if(!genres){
        res.status(400).send("ID not found")
        return
    }

    res.send(genres)
})

// delete genre
router.delete('/:id', [auth, admin], async (req, res) => {

    // delete genre
    const genres = await Genre.findByIdAndRemove(
        req.params.id
    )

    // check genre id is exists or not
    if(!genres){
        res.status(404).send("ID not found")
        return
    }

    res.send(genres)
})

module.exports = router;
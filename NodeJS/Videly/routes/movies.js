const {Movie, validateBodyData} = require('../models/movies')
const express = require("express")
const router = express.Router()

// get all genres
router.get('/', async (req, res) => {
    const movies = await Movie.find().sort('title')
    res.send(movies)
})

// get particular genres
router.get('/:id', async (req, res) => {

    // get genres 
    const movies = await Movie.findById(
        req.params.id
    )

    // check the genre id is existing or not
    if(!movies){
        res.status(400).send("ID Not Found")
        return
    }

    res.send(movies)
})

// create new genres
router.post('/', async (req, res) => {

    // validate body data
    const {error} = validateBodyData(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    // create genres
    let movies = new Movie({
        title: req.body.title,
        genre: {
            _id: genre._id,
            name: genre.name
        },
        numberInStock: req.body.numberInStock,
        dailyRentalRate: req.body.dailyRentalRate
    })
    movies = await movies.save()
    res.send(movies)
})

// edit existing genre
router.put('/:id', async (req, res) => {

    // validate body data
    const {error} = validateBodyData(req.body)
    if(error){
        res.status(404).send(error.details[0].message)
        return
    }

    // update data
    const movies = await Movie.findByIdAndUpdate(
        req.params.id,
        {
            title: req.body.title,
            genre: {
                _id: genre._id,
                name: genre.name
            },
            numberInStock: req.body.numberInStock,
            dailyRentalRate: req.body.dailyRentalRate,
            new: true
        }       
    )

    // check the genre id is existing or not
    if(!movies){
        res.status(400).send("ID not found")
        return
    }

    res.send(movies)
})

// delete genre
router.delete('/:id', async (req, res) => {

    // delete genre
    const movies = await Movie.findByIdAndRemove(
        req.params.id
    )

    // check genre id is exists or not
    if(!movies){
        res.status(404).send("ID not found")
        return
    }

    res.send(movies)
})

module.exports = router;
const mongoose = require('mongoose')

// Connect with MongoDB and Create Database (playground)
mongoose.connect("mongodb://localhost/playground")
    .then(() => console.log("Connected to MongoDB..."))
    .catch(err => console.error("Connection Fails...", err))

// Schema creation (table+Schema) for table raws
const courseSchema = mongoose.Schema({
    name: {
        type: String, 
        required: true,
        minlength: 5,
        maxlength: 255
    },
    category: {
        type: String,
        required: true,
        enum: ['web', 'mobile'],
        lowercase: true
        // uppercase: true
    },
    author: String,
    tags: {
        type: Array,
        validate: {
            validator: function(v){
                return v && v.length > 0;
            },
            message: 'A course should have at leat one tag.'
        }
    },
    date: {type: Date, default: Date.now},
    isPublished: Boolean,
    price: {
        type: Number,
        required: function(){
            return this.isPublished;
        },
        get: v => Math.round(v),
        set: v => Math.round(v) 
    }
});

// Table (Course) or Model Creation
const Course = mongoose.model('Course', courseSchema)

// async function for course creation
async function courseCreation(){

    // Assign value to Table properties Or raw creation
    const course = new Course({
        name: 'Angular Course',
        category: 'Web',
        author: 'Dhrumil',
        tags: ['angular', 'backend'],
        isPublished: true,
        price: 45.59
    })

    // Save Table
    try {
        const result = await course.save();
        console.log("my result: ", result)
    }
    catch(ex){
        for(error_key in ex.errors){
            console.log(ex.errors[error_key].message)
        }
        // console.log(ex)
    }
    
}

// fetch data from the table
async function getCourses(){
    const courses = await Course
    // comparison query operator
    // le, gt, lte, gte, in, nin
        // .find({price: {$gte: 4, $lte: 18}})
        // .find({price: {$in: [10, 15]}})
        // .find({author: 'Dhrumil', isPublished: true})
        .find({_id: '620207457d46e29261a04ef2'})

        // logical operator
        // .or([{name: Dhrumil}, {isPublished: true}])
        // .and([{name: Dhrumil}, {isPublished: true}])

        // Regular Expression
        // .find({author: /^Dhru/}) // Author name starts with Dhr
        // .find({author: /mil$/}) // Author name ends with mil
        // .find({author: /.*Dhrumil.*/}) // Author name contains Dhrumil
        // .find({author: /.*Dhrumil.*/i}) // write i at the end that make non case-sensitive

        // .limit(10)
        .sort({name: 1}) // 1: Aescending, -1: Descending
        // .count() // count the result documents or raw
        .select({name: 1, tags: 1, price: 1});
    console.log(courses[0].price)
}

// This all process is Asynchronous, so make it async and call
// courseCreation();

// call getCourses function
getCourses();

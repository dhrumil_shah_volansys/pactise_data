const mongoose = require('mongoose')

// connect with database
mongoose.connect("mongodb://localhost/mongo_exercise")
    .then(() => console.log("Connected......."))
    .catch(err => console.error("Connection Fail.........", err))

// schema creation
const mongoExerciseSchema = new mongoose.Schema({
    tags: [String],
    date: {type: Date, default: Date.now},
    name: String,
    author: String,
    isPublished: Boolean,
    price: Number
})

// table creation
const Courses = mongoose.model('Courses', mongoExerciseSchema)

// fatch data from the table
// async function getCourses(){
//     const result = await Courses

//         // Exercise -> 1
//         // .find({isPublished: true, tags: 'backend'})
//         // .limit(10)
//         // .sort({name: 1})
//         // .select({name: 1, author: 1})

//         // Exercise -> 2
//         // .find({isPublished: true, tags: {$in: ['frontend', 'backend']}})
//         // .sort('-price')
//         // .select('name author')

//         // Exercise -> 3
//         .find({isPublished: true})
//         .or([{price: {$gte: 15}}, {name: /.*by.*/i}])
//     console.log(result)
// }

// update data
// async function updateCourses(id){
//     const result = await Courses.findOneAndUpdate(id, {
//         $set: {
//             author: 'Dhrumil',
//             isPublished: false
//         }
//     }, {new: true})
//     console.log(result)
// }

// delete record
async function deleteCourse(id){
    const result = await Courses.findByIdAndDelete(id)
    console.log(result)
}


// getCourses();
// updateCourses('5a68fdd7bee8ea64649c2777');
deleteCourse('5a68fde3f09ad7646ddec17e');
const lib = require('../lib')

describe('absolute', () => {
    it('should return positive number if value is positive', () => {
        const result = lib.absolute(1)
        expect(result).toBe(1)
    })
    
    it('should return positive number if value is negative', () => {
        const result = lib.absolute(-1)
        expect(result).toBe(1)
    })
    
    it('should return 0 if value is 0', () => {
        const result = lib.absolute(0)
        expect(result).toBe(0)
    })
})

describe('greet', () => {
    it('should return string', () => {
        const result = lib.greet("Dhrumil")
        expect(result).toMatch(/Dhrumil/)
    })
})

describe('getCurrencies', () => {
    it('should return arraylist', () => {
        const result = lib.getCurrencies()
        expect(result).toEqual(expect.arrayContaining(['EUR', 'USD', 'AUD']))
    })
})

describe('getproduct', () => {
    it('should return object', () => {
        const result = lib.getProduct(1)
        expect(result).toMatchObject({id: 1, price: 10})
    })
})

describe('registerUser', () => {
    it('should throw if username is falsy', () => {
        const args = [null, 0, NaN, '', false, undefined]
        args.forEach(a => {
            expect(() => {
                lib.registerUser(a)
            }).toThrow()
        })
    })

    it('should return object i valid username is passed', () => {
        const result = lib.registerUser('djshah')
        expect(result).toMatchObject({username: 'djshah'})
        expect(result.id).toBeGreaterThan(0)
    })
})
const http = require('http')

const server = http.createServer((req, res) => {
    if(req.url === '/'){
        res.write("request sended...")
        res.end()
    }
    if(req.url === '/jsondata'){
        res.write(JSON.stringify([1, 2, 3]))
        res.end()
    }
})

server.listen(3000)

console.log("server listening....")
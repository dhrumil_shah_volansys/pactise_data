console.log("Before")
getUser(4, (user) => {
    console.log("User", user)
    getGitRepo((repo) => {
        console.log("Repo", repo)
    })
})
console.log("After")

function getUser(id, callback){
    setTimeout(() => {
        console.log("Set time out")
        callback({id: id, username: "djshah"})
    }, 3000)    
    console.log(`id: ${id}`)
}

function getGitRepo(callback){
    setTimeout(() => {
        console.log("getGitRepo")
        callback(['repo1', 'repo2', 'repo3'])
    }, 3000)
}

// RBIPTRN203770875
const EventEmitter = require('events')

class Logger extends EventEmitter{
    log(msg){
        console.log("Log function call " + msg)
        this.emit('logevent', {'message': 'log event message'})
    }
}

module.exports = Logger
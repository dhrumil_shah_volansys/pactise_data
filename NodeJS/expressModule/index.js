const config = require("config")
const Joi = require('joi')
const logger = require('./logger')
const express = require('express');
const app = express();

// middleware
app.use(express.json());

// custom middleware
app.use((req, res, next) => {
    console.log("Middleware")
    next()
})

// Environments
// console.log(`NODE_ENV: ${process.env.NODE_ENV}`)
// console.log(`app: ${app.get('env')}`)
if (app.get('env') === 'development'){
    console.log('development')
}

// Configuration
console.log(`Application Name: ${config.get('name')}`)
console.log(`Mail Host Name: ${config.get('mail.host')}`)
console.log(`Mail Password: ${config.get('mail.password')}`)

// Template Engine
app.set('view engine', 'pug')

var courses = [
    {id: 1, name: 'course1'},
    {id: 2, name: 'course2'},
    {id: 3, name: 'course3'},
]

app.get('/', (req,res) => {
    res.render('index', {title: 'My express app', message: 'hello'})
    // res.send("Hello World....")
})

// get all courses
app.get('/api/courses', (req, res) => {
    res.send(courses)
})

// handle get request
app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(
        c => c.id === parseInt(req.params.id)
    )
    if (!course){
        res.status(404).send("ID not found")
    }
    res.send(course)
})

// handle post request
app.post('/api/courses', (req, res) => {
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    })
    const result = schema.validate(req.body)
    console.log(result)
    if (result.error){
        res.status(400).send(result.error.details[0].message)
        return;
    }
    const course = {
        id: courses.length + 1,
        name: req.body.name,
    };
    courses.push(course)
    res.send(course)
});

// schema validation
function validateSchema(course){
    const schema = Joi.object({
        name: Joi.string().min(3).required()
    })
    return schema.validate(course)
}

// handle put request
app.put('/api/courses/:id', (req, res) => {

    // check course is exist or not
    const course = courses.find(c => c.id === parseInt(req.params.id))
    if (!courses){
        res.status(404).send("ID not found")
        return;
    }
    
    // check updated data in request
    const result = validateSchema(req.body)
    if (result.error){
        res.status(400).send(result.error.details[0].message)
        return
    }

    // update changes
    course.name = req.body.name
    res.send(course)
})

// handle delete request
app.delete('/api/courses/:id', (req, res) => {

    // check the course is exists or not
    const course = courses.find(c => c.id === parseInt(req.params.id))
    if(!course){
        res.status(400).send("ID not found")
        return;
    }

    // delete course
    const index = courses.indexOf(course)
    courses.splice(index, 1)
    res.send(course)
})

// assign port dynamically by set {export PORT=5000}
const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log(`Listening on ${port}......`)
})
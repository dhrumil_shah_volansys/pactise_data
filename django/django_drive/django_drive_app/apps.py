from django.apps import AppConfig


class DjangoDriveAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'django_drive_app'

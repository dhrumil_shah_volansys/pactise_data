from django.shortcuts import render
import pkg_resources
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Student
from .serializer import StudentSerializer
from rest_framework import status

# Create your views here.

class StudentAPI(APIView):
    def get(self, request, pk=None, format=None):
        id = pk
        if id is not None:
            students = Student.objects.get(id=id)
            serializer = StudentSerializer(students)
            return Response(serializer.data)
        students = Student.objects.all()
        serializer = StudentSerializer(students, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg': 'Data Created'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_404_BAD_REQUEST)


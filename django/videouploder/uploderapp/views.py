from django.shortcuts import render, redirect
from .forms import VideoUploadForm
import ntpath

# Create your views here.
def home(request):
    return render(request, 'uploderapp/home.html')

# def index(request):
#     if request.method == 'POST':
#         form = VideoUploadForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             print('Uploded file: ', request.FILES)
#             return redirect('home')
#     else:
#         form = VideoUploadForm()
#     return render(request, 'uploderapp/index.html', {
#         'form': form
#     })

import boto3
from boto3.s3.transfer import TransferConfig


s3_client = boto3.client('s3')

S3_BUCKET = 'djangovideouploder'
FILE_PATH = '/path/to/file/'
KEY_PATH = "VNCVPpbV+56j/DGHd1D6+i5RrWTPFfJA2HbUnYVa" 

def uploadFileS3(filename):
    config = TransferConfig(multipart_threshold=1024*25, max_concurrency=10,
                        multipart_chunksize=1024*25, use_threads=True)
    file = FILE_PATH + filename
    key = KEY_PATH + filename
    s3_client.upload_file(file, S3_BUCKET, key,
    ExtraArgs={ 'ACL': 'public-read', 'ContentType': 'video/mp4'},
    Config = config,
    Callback=ProgressPercentage(file)
    )

def handle_uploaded_file(f):
    with open('uploderapp/temp/demo.mp4', 'wb+') as destination:
        for chunk in f.chunks():
             destination.write(chunk)

def index(request):
    if request.method == 'POST':
        form = VideoUploadForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['video'])
            uploadFileS3(ntpath.basename('demo/'))
            return redirect('home')
    else:
        form = VideoUploadForm()
    return render(request, 'uploderapp/index.html', {
        'form': form
    })


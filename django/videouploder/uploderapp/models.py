from django.db import models

# Create your models here.
class Videos(models.Model):
    name = models.CharField(max_length=180)
    video = models.FileField(upload_to='media/')
from django import forms
from .models import Videos

class VideoUploadForm(forms.ModelForm):
    class Meta:
        model = Videos
        fields = ('name', 'video')
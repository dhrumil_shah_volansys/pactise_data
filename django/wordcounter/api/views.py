from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .models import Counter
from .serializers import CounterSerializer
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
def index(request):
    return JsonResponse({'info': 'count word api v1', 'developer': 'Dhrumil'})

@api_view(['GET', 'POST'])
def count_word(request):

    if request.method == 'GET':
        data = Counter.objects.all()
        serializer = CounterSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        print('request.data: ', request.data)
        if not 'text' in request.data.keys():
            return Response({'detail': 'No text parameter found'}, status=status.HTTP_400_BAD_REQUEST)
        data = request.data
        data['total_words'] = len(data['text'].split())
        serializer = CounterSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def each_word_count(request, pk):

    # Check requested id is exist or not 
    try:
        record = Counter.objects.get(id=pk)
    except Counter.DoesNotExist:
        return Response({'detail': 'No such record exists'}, status=status.HTTP_400_BAD_REQUEST)

    # Response requested id data 
    if request.method == 'GET':
        serializer = CounterSerializer(record)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Update and response requested id data 
    elif request.method == 'PUT':
        if not 'text' in request.data.keys():
            return Response({'detail': 'No text parameter found'}, status=status.HTTP_400_BAD_REQUEST)
        data = request.data
        data['total_words'] = len(data['text'].split())
        serializer = CounterSerializer(record, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    # Delete requested id 
    elif request.method == 'DELETE':
        record.delete()
        return Response({'detail': 'Record Deleted'}, status=status.HTTP_200_OK)
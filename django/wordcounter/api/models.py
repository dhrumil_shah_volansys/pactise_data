from django.db import models

# Create your models here.
class Counter(models.Model):
    text = models.TextField()
    total_words = models.PositiveIntegerField(default=0)

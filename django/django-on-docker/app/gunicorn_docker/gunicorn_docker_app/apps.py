from django.apps import AppConfig


class GunicornDockerAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gunicorn_docker_app'

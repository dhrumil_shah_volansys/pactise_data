from django.shortcuts import render

# Create your views here.

def setSession(request):
    request.session['name'] = 'dhrumil'
    return render(request, 'session_app/setSession.html')

def getSession(request):
    name = request.session.get('name')
    return render(request, 'session_app/getSession.html', {'name': name})

def deleteSession(request):
    request.session.flush()
    return render(request, 'session_app/deleteSession.html')

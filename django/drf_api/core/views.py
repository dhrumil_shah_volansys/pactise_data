from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from .serializers import PostSerializer
from core.models import Post

# Create your views here.
class TestView(APIView):

    permission_classes = (IsAuthenticated,  )

    def get(self, request, *args, **kwargs):
        alldata = Post.objects.all()
        serializer = PostSerializer(alldata, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
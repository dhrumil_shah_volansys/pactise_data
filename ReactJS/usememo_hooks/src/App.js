import logo from './logo.svg';
import './App.css';
import UsememoHooks from './components/UsememoHooks';

function App() {
  return (
    <div className="App">
      <UsememoHooks/>
    </div>
  );
}

export default App;

import React, {useState} from "react";

const HooksCounterTwo = () => {

    const initialCount = 0
    const [count, setCount] = useState(initialCount)

    return(
        <div>
            count: {count}
            <button onClick={() => setCount(initialCount)}>Reset</button>
            <button onClick={() => setCount(prevCount => prevCount + 1)}>Increment</button>
            <button onClick={() => setCount(prevCount => prevCount - 1)}>Decrement</button>
            <button onClick={() => setCount(prevCount => prevCount + 5)}>Increment by 5</button>
        </div>
    )
}

export default HooksCounterTwo
import React, {useState, useEffect} from "react";

const HooksMouse = () => {

    const [x, setX] = useState(0)
    const [y, setY] = useState(0)

    const setMouseCoordinates = (event) => {
        console.log("Coordinates is setted")
        setX(event.clientX)
        setY(event.clientY)
    }

    useEffect(() => {
        console.log("useeffect is called")
        window.addEventListener('mousemove', setMouseCoordinates)

        return () => {
            window.removeEventListener('mousemove', setMouseCoordinates)
        }
    }, [])

    return(
        <div>
            X - {x}, Y - {y}
        </div>
    )
}

export default HooksMouse
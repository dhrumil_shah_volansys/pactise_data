import React, { useContext } from "react";
import {CounterContext} from '../App'

const ComponentD = () => {

    const counterContext = useContext(CounterContext)

    return(
        <div>
            <button onClick={() => counterContext.userDispatch('increment')}>Increment</button>
            <button onClick={() => counterContext.userDispatch('decrement')}>Decrement</button>
            <button onClick={() => counterContext.userDispatch('reset')}>Reset</button>
        </div>
    )
}

export default ComponentD
import axios from "axios";
import React, { useEffect, useReducer } from "react";

const initialState = {
    loading: true,
    error: '',
    post: {}
}

const reducer = (state, action) => {
    switch(action.type){
        case 'fetch_success':
            return {
                loading: false,
                error: '',
                post: action.payload
            }
        case 'fetch_error':
            return {
                loading: false,
                error: 'Something Went wrong!',
                post: {}
            }
        default: 
            return state
    }
}

const DataFetching = () => {

    const [state, dispatch] = useReducer(reducer, initialState)

    useEffect(() => {
        axios
            .get('https://jsonplaceholder.typicode.com/posts/1')
            .then(response => {
                dispatch({type: 'fetch_success', payload: response.data})
            })
            .catch(error => {
                dispatch({type: 'fetch_error'})
            })
    }, [])

    return(
        <div>
            {state.loading ? 'Loading' : state.post.title}
            {state.error ? state.error : null}
        </div>
    )
}

export default DataFetching
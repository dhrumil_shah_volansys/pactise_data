import axios from "axios";
import React, { useEffect, useState } from "react";

const UseEffectGet = () => {

    const [posts, setPost] = useState([])
    const [id, setId] = useState(1)
    const [idFromButtonClick, setIdFromButtonClick] = useState(1)

    const handleClick = () => {
        setIdFromButtonClick(idFromButtonClick)
    }

    useEffect(async () => {
        const response = await axios.get(`https://jsonplaceholder.typicode.com/posts/${idFromButtonClick}`)
        console.log(response)
        setPost(response.data)
    }, [idFromButtonClick])

    return(
        <div>
            <input type="text" value={id} onChange={event => setId(event.target.value)}/>
            <button onClick={handleClick}>Fetch Title</button>
            {/* <ul>
                {posts.map(post => <li key={post.id}>{post.title}</li>)}
            </ul> */}
            {posts.title}
        </div>
    )
}

export default UseEffectGet
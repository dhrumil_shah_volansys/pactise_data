import React, {useState} from "react";

const HooksArray = () => {

    const [items, setItems] = useState([])

    const addItem = () => {
        setItems([ ...items, {id: items.length, 
        value: Math.random()}])
    }

    return(
        <div>
            <button onClick={addItem}>ADD</button>
            <ul>
                {items.map(item => <li key={item.id}>{item.value}</li>)}
            </ul>
        </div>
    )
}

export default HooksArray
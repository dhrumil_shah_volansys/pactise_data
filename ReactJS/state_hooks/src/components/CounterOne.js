import React, { useReducer } from "react";

// Define initialState of useReducer 
const initialState = {
    firstCount: 0,
    secondCount: 10
}

// Define reducer function of useReducer
const reducer = (state, action) => {
    switch(action.type){
        case 'increment':
            return {...state, firstCount: state.firstCount + action.value}
        case 'decrement':
            return {...state, firstCount: state.firstCount - action.value}
        case 'increment2':
            return {...state, secondCount: state.secondCount + action.value}
        case 'decrement2':
            return {...state, secondCount: state.secondCount - action.value}
        case 'reset':
            return initialState
        default:
            return state
    }
}

// Component 
const CounterOne = () => {

    const[count, dispatch] = useReducer(reducer, initialState)

    return(
        <div>
            <h1>First Count = {count.firstCount} || Second Count = {count.secondCount}</h1>
            <button onClick={() => dispatch({type: 'increment', value: 1})}>Increment</button>
            <button onClick={() => dispatch({type: 'decrement', value: 1})}>Decrement</button>
            <button onClick={() => dispatch({type: 'increment', value: 5})}>Increment5</button>
            <button onClick={() => dispatch({type: 'decrement', value: 5})}>Decrement5</button>
            <button onClick={() => dispatch({type: 'reset'})}>Reset</button>
            <div>
                <button onClick={() => dispatch({type: 'increment2', value: 1})}>Increment</button>
                <button onClick={() => dispatch({type: 'decrement2', value: 1})}>Decrement</button>
            </div>
        </div>
    )
}

export default CounterOne
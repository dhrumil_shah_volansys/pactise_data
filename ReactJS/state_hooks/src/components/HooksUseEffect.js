import React, {useState, useEffect} from "react";

const HooksUseEffect = () => {

    const [count, setCount] = useState(0)

    useEffect(() => {
        document.title = `clicked ${count} times`
    })

    return(
        <div>
            <button onClick={() => setCount(count + 1)}>clicked {count} times</button>
        </div>
    )
}

export default HooksUseEffect
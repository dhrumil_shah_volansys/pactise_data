import React, {useReducer} from 'react';
// import logo from './logo.svg';
import './App.css';
import DataFetching from './components/DataFetching'
// import HooksCounter from './components/HooksCounter';
// import HooksCounterTwo from './components/HooksCounterTwo';
// import HooksObject from './components/HooksObject';
// import HooksArray from './components/HooksArray';
// import HooksUseEffect from './components/HooksUseEffect';
// import HooksMouse from './components/HooksMouse';
// import MouseContainer from './components/MouseContainer';
// import IntervalHooksCount from './components/IntervalHooksCount';
// import UseEffectGet from './components/UseEffectGet';
// import CounterOne from './components/CounterOne';
// import ComponentA from './components/ComponentA'
// import ComponentB from './components/ComponentB'
// import ComponentC from './components/ComponentC'

// export const CounterContext = React.createContext()
// const initialState = 0

// const reducer = (state, action) => {
//   switch(action){
//     case 'increment':
//       return state + 1
//     case 'decrement':
//       return state - 1
//     case 'reset':
//       return initialState
//     default:
//       return state
//   }
// }


function App() {

  // const [count, dispatch] = useReducer(reducer, initialState)

  // return (
  //   <CounterContext.Provider value={{userCount: count, userDispatch: dispatch}}>
  //     <div className="App">
  //       {/* <HooksCounter/> */}
  //       {/* <UseEffectGet/> */}

  //       {/* useReducer  */}
  //       {/* <CounterOne/> */}

  //       <h1>Counter = {count}</h1>
  //       <ComponentA/>
  //       <ComponentB/>
  //       <ComponentC/>
  //     </div>
  //   </CounterContext.Provider>
  // );

  return(
    <div className="App">
      <DataFetching/>
    </div>
  )
}

export default App;

import logo from './logo.svg';
import './App.css';
import Greet from './components/Greet';
import Welcome from './components/Welcome';
import Counter from './components/Counter';
import FunctionClick from './components/FunctionClick';
import ClassClick from './components/ClassClick';
import EventBinding from './components/EventBinding'
import ParentComponent from './components/ParentComponent';
import UserGreeting from './components/UserGreeting';
import NameList from './components/NameList';
import StyleSheet from './components/StyleSheet';
import './appStyles.css'
import styles from './appStyles.module.css'
import Form from './components/Form';

function App() {
  return (
    <div className="App">
      {/* <Greet name='Dhrumil' surname='Shah'>
        <p>This is paragraph</p>
      </Greet>
      <Greet name='Biren' surname='Patel'>
        <button>Action</button>
      </Greet>
      <Greet name='Amit' surname='Vaghela'/>
      <Welcome/>
      <Counter/>
      <FunctionClick/>
      <ClassClick/>
      <EventBinding/>
      <ParentComponent/> */}
      {/* <UserGreeting/> */}
      {/* <NameList/> */}
      {/* <h1 className='error'>Error</h1>
      <h1 className={styles.success}>Success</h1>
      <StyleSheet primary={true}/> */}
      <Form/>
    </div>
  );
}

export default App;

import React, {Component} from "react";

class ClassClick extends Component{

    classClick = () => {
        console.log('Clicked...')
    }

    render(){
        return(
            <div>
                <button onClick={this.classClick}>ClassClick</button>
            </div>
        )
    }
}

export default ClassClick
import React from "react";

const ChildComponent = (props) => {
    return(
        <div>
            <button onClick={() => props.clickHandler('child')}>Child Click</button>
        </div>
    )
}

export default ChildComponent
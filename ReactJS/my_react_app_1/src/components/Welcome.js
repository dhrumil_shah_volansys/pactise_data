import React, {Component} from "react";

class Welcome extends Component{

    constructor(){
        super()
        this.state = {
            message: "Please Subscribe"
        }
    }

    changeState(){
        this.setState({
            message: "Thank You for Subscribe"
        })
    }

    render(){
        return(
            <div>
                <h1>{this.state.message}</h1>
                <button onClick={() => this.changeState()}>Subscribe</button>
            </div>
        )
    }
}

export default Welcome
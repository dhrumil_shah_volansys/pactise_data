import React, {Component} from "react";

class Counter extends Component{

    constructor(){
        super()
        this.state = {
            count: 0
        }
    }

    incrementCount(){
        this.setState((prevState) => ({
            count: prevState.count + 1
        }))
    }

    incrementFiveTime(){
        this.incrementCount()
        this.incrementCount()
        this.incrementCount()
        this.incrementCount()
    }

    render(){
        return(
            <div>
                <h1>Count - {this.state.count}</h1>
                <button onClick={() => this.incrementFiveTime()}>Increment</button>
            </div>
        )
    }
}

export default Counter
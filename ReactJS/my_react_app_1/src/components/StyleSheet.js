import React from "react";
import './StyleSheet.css'

const StyleSheet = (props) => {

    const className = props.primary ? 'primary' : ''
    return(
        <div>
            <h1 className={`${className} font-xl`}>StyleSheet</h1>
        </div>
    )
}

export default StyleSheet
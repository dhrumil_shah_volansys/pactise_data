import React, {Component} from "react";

class UserGreeting extends Component{

    constructor(){
        super()
        this.state = {
            isLoggedIn: false
        }
    }

    render(){
        if (this.state.isLoggedIn){
            return(<div>Inside IF</div>)
        }else{
            return(<div>Inside ELSE</div>)
        }
    }
}

export default UserGreeting

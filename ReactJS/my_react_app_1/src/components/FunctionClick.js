import React from "react";

const FunctionClick = () => {

    const funClick = () => {
        console.log('Clicked..')
    }

    return(
        <div>
            <button onClick={funClick}>Function Click</button>
        </div>
    )
}

export default FunctionClick
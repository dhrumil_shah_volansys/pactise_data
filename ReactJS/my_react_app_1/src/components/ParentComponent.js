import React, {Component} from "react";
import ChildComponent from "./ChildComponent";

class ParentComponent extends Component{

    constructor(){
        super()
        this.state = {
            parentName: 'Parent'
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(childName){
        alert(`Hello ${this.state.parentName} this click from ${childName}`)
    }

    render(){
        return(
            <ChildComponent clickHandler={this.handleClick}/>
        )
    }
}

export default ParentComponent

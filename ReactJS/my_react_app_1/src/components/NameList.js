import React from "react";
import Person from "./Person";

const NameList = () => {
    let persons = [
        {
            id: 1,
            name: 'Dhrumil',
            age: 34,
            skill: 'Python'
        },
        {
            id: 2,
            name: 'Amit',
            age: 35,
            skill: 'Java'
        },
        {
            id: 3,
            name: 'Biren',
            age: 36,
            skill: 'IELTS'
        }
    ]

    let personList = persons.map(person => <Person key={person.id} person={person}/>)
    return <div>{personList}</div>
}

export default NameList

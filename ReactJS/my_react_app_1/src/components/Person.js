import React from "react";

const Person = ({person}) => {
    return(
        <div>
            <h1>My name is {person.name} and i am {person.age} years old. I have skill in {person.skill}</h1>
        </div>
    )
}

export default Person
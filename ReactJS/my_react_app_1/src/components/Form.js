import React, {Component} from "react";

class Form extends Component{

    constructor(){
        super()
        this.state = {
            username: '',
            comments: '',
            topic: 'angular'
        }
    }

    handleUserName = (event) => {
        this.setState({
            username: event.target.value
        })
    }

    handleComments = (event) => {
        this.setState({
            comments: event.target.value
        })
    }

    handleTopics = (event) => {
        this.setState({
            topic: event.target.value
        })
    }

    handleForm = (event) => {
        alert(`${this.state.username} ${this.state.comments} ${this.state.topic}`)
        event.preventDefault()
    }

    render(){
        return(
            <form onSubmit={this.handleForm}>
                <div>
                    <label>UserName</label>
                    <input type='text' value={this.state.username} onChange={this.handleUserName}/>
                </div>
                <div>
                    <label>Comments</label>
                    <textarea value={this.state.comments} onChange={this.handleComments}/>
                </div>
                <div>
                    <label>Topics</label>
                    <select value={this.state.topic} onChange={this.handleTopics}>
                        <option value='react'>ReactJS</option>
                        <option value='angular'>Angular</option>
                        <option value='node'>NodeJS</option>
                    </select>
                </div>
                <button type="submit">Submit</button>
            </form>
        )
    }
}

export default Form
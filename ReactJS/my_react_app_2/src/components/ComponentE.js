import React, {Component} from "react";
import ComponentF from "./ComponentF";
import UserContext from "./UserContext";

class ComponentE extends Component{
    render(){
        return(
            <div>
                <h1>Comp e {this.context}</h1>
                <ComponentF/>       
            </div>
        )
    }
}

ComponentE.contextType = UserContext

export default ComponentE
import React from "react";

const ErrorDemo = ({heroName}) => {
    if (heroName === 'Joker'){
        throw new Error('Not a Hero');
    }
    return(
        <div>
            {heroName}
        </div>
    )
}

export default ErrorDemo
import React, {Component} from "react";
import MemoComp from "./MemoComp";
import PureComp from "./PureComp";
import RegComp from "./RegComp";

class ParentComp extends Component{

    constructor(){
        super()
        this.state = {
            name: 'Dhrumil'
        }
    }

    componentDidMount(){
        setInterval(() => {
            this.setState({
                name: 'Dhrumil'
            })
        }, 2000)
    }

    render(){
        console.log('********ParentComp render*********')
        return(
            <div>
                <h1>Parent Component</h1>
                {/* <RegComp name={this.state.name}/>
                <PureComp name={this.state.name}/> */}
                <MemoComp name={this.state.name}/>
            </div>
        )
    }
}

export default ParentComp
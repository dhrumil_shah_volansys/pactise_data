import React, {Component} from "react";

class Input extends Component{

    constructor(){
        super()

        // Below statement contain refrence data
        this.inputRef = React.createRef()
    }

    focusInput(){
        this.inputRef.current.focus()
    }

    render(){
        return(
            <input type='text' ref={this.inputRef}/>
        )
    }
}

export default Input
import React from "react";
import FragmentColumn from "./FragmentColumn";

const FragmentTable = () => {
    return(
        <table>
            <tbody>
                <tr>
                    <FragmentColumn/>
                </tr>
            </tbody>
        </table>
    )
}

export default FragmentTable
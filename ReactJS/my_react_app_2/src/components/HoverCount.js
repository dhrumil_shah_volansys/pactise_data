import React, {Component} from "react";
import UpdatedComponent from "./withCounter";

class HoverCount extends Component{

    render(){
        const {count, incrementCount} = this.props
        return(
            <div>
                <h2 onMouseOver={incrementCount}>Hover {count} Times</h2>
            </div>
        )
    }
}

export default UpdatedComponent(HoverCount, 10)
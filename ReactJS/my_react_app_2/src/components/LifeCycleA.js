import React, {Component} from "react";
import LifeCycleB from "./LifeCycleB";

class LifeCycleA extends Component{
    
    constructor(){
        super()
        this.state = {
            message: 'Hello'
        }
        console.log('LifeCycleA Constructor')
    }

    static getDerivedStateFromProps(props, state){
        console.log('LifeCycleA getDerivedStateFromProps')
        return null
    }

    componentDidMount(){
        console.log('LifeCycleA componentDidMount')
    }

    shouldComponentUpdate(){
        console.log('LifeCycleA shouldComponentUpdate')
        return true
    }

    getSnapshotBeforeUpdate(prevProps, prevState){
        console.log('LifeCycleA getSnapshotBeforeUpdate')
        return null
    }

    componentDidUpdate(){
        console.log('LifeCycleA componentDidUpdate')
    }

    changeState = () => {
        this.setState({
            message: 'GoodBye!'
        })
    }

    render(){
        console.log('LifeCycleA render')
        return(
            <div>
                <h1>LifeCycleA</h1>
                <button onClick={this.changeState}>Change State</button>
                <LifeCycleB/>
            </div>
        )
    }
}

export default LifeCycleA
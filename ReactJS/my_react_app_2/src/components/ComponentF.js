import React, {Component} from "react";
import { ContextConsumer } from "./UserContext";

class ComponentF extends Component{
    render(){
        return(
            <div>
                <ContextConsumer>
                    {
                        (username) => {
                            return <h1>Hello {username}</h1>
                        }
                    }
                </ContextConsumer>
            </div>
        )
    }
}

export default ComponentF
import logo from './logo.svg';
import './App.css';
import LifeCycleA from './components/LifeCycleA';
import FragmentTable from './components/FragmentTable';
import ParentComp from './components/ParentComp';
import RefsDemo from './components/RefsDemo';
import FocusInput from './components/FocusInput';
import FRInputParent from './components/FRInputParent';
import PortalDemo from './components/PortalDemo';
import ErrorBoundry from './components/ErrorBoundry';
import ErrorDemo from './components/ErrorDemo';
import ClickCount from './components/ClickCount';
import HoverCount from './components/HoverCount';
import WithCounterTwo from './components/withCounterTwo';
import ClickCountTwo from './components/ClickCountTwo';
import HoverCountTwo from './components/HoverCountTwo';
import {ContextProvider} from './components/UserContext'
import ComponentC from './components/ComponentC';

function App() {
  return (
    <div className="App">
      {/* <LifeCycleA/> */}
      {/* <FragmentTable/> */}
      {/* <ParentComp/> */}
      {/* <RefsDemo/> */}
      {/* <FocusInput/> */}
      {/* <FRInputParent/> */}
      {/* <PortalDemo/> */}
      {/* <ErrorBoundry>
        <ErrorDemo heroName="ab"/>
      </ErrorBoundry>
      <ErrorBoundry>
        <ErrorDemo heroName="cd"/>
      </ErrorBoundry>
      <ErrorBoundry>
        <ErrorDemo heroName="Joker"/>
      </ErrorBoundry> */}
      {/* <ClickCount/>
      <HoverCount/> */}
      {/* <WithCounterTwo render={(count, incrementCount) => (
        <ClickCountTwo count={count} incrementCount={incrementCount}/>
      )}/>
      <WithCounterTwo render={(count, incrementCount) => (
        <HoverCountTwo count={count} incrementCount={incrementCount}/>
      )}/> */}
      <ContextProvider value="Dhrumil">
        <ComponentC/>
      </ContextProvider>
    </div>
  );
}

export default App;

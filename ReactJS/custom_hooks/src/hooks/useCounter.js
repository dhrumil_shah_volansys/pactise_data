import {useState} from "react";

const useCounter = (initialCount = 0, value) => {

    const [count, setCount] = useState(initialCount)

    const incrementCount = () => {
        setCount(count + value)
    }

    const decrementCount = () => {
        setCount(count - value)
    }

    const resetCount = () => {
        setCount(initialCount)
    }

    return [incrementCount, decrementCount, resetCount, count]
}

export default useCounter
import React, { useEffect, useState } from "react";
import useDocumentTitle from "../hooks/useDocumentTitle";

const DocumentTitle = () => {

    // value update in count, setCount is function 
    const [count, setCount] = useState(0)

    // custom Hook
    useDocumentTitle(count)

    return(
        <div>
            <button onClick={() => setCount(count + 1)}>count - {count}</button>
        </div>
    )
}

export default DocumentTitle
import React from "react";
import useCounter from "../hooks/useCounter";

const CounterOne = () => {

    const [incrementCount, decrementCount, resetCount, count] = useCounter(0, 2)

    return(
        <div>
            <h1>Count - {count}</h1>
            <button onClick={incrementCount}>Increment</button>
            <button onClick={decrementCount}>Decrement</button>
            <button onClick={resetCount}>Reset</button>
        </div>
    )
}

export default CounterOne
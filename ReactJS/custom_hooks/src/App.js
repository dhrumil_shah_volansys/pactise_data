import logo from './logo.svg';
import './App.css';
import DocumentTitle from './components/DocumentTitle';
import CounterOne from './components/CounterOne';
import CounterTwo from './components/CounterTwo';

function App() {
  return (
    <div className="App">
      {/* <DocumentTitle/> */}
      <CounterOne/>
      <CounterTwo/>
    </div>
  );
}

export default App;

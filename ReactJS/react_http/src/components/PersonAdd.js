import React, {Component} from "react";
import axios from "axios";

class PersonAdd extends Component{

    state = {
        name: ''
    }

    inputHandle = event => {
        this.setState({
            name: event.target.value
        })
    }

    submitHandle = async event => {
        event.preventDefault()
        const addUser = {
            name: this.state.name
        }
        const response = await axios.post(`https://jsonplaceholder.typicode.com/users`, { addUser })
        console.log(response)
    }

    render(){
        return(
            <div>
                <form onSubmit={this.submitHandle}>
                    <label>
                        Person Name: 
                        <input type='text' name='name' onChange={this.inputHandle}/>
                    </label>
                    <button type="submit">ADD</button>
                </form>
            </div>
        )
    }
}

export default PersonAdd
import React, {Component} from "react";
import axios from "axios";

class PersonList extends Component{

    constructor(){
        super()
        this.state = {
            persons: []
        }
    }

    async componentDidMount(){
        const personData = await axios.get(`https://jsonplaceholder.typicode.com/users`)
        this.setState({
            persons: personData.data
        })
    }

    render(){
        return(
            <div>
                <ul>
                    {this.state.persons.map(person => <li key={person.id}>{person.name}</li>)}
                </ul>
            </div>
        )
    }
}

export default PersonList
import React from "react";

const Title = ({callbackTitle}) => {
    return(
        <h1>{callbackTitle}</h1>
    )
}

export default Title
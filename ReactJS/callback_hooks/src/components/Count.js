import React from "react";

const Count = ({text, count}) => {
    console.log(`${text} count call`)
    return(
        <div>
            <h2>{text} = {count}</h2>
        </div>
    )
}

export default React.memo(Count)
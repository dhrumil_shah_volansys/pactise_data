import React from "react";

const Button = ({clickHandler, children}) => {
    console.log(`${children} button call`)
    return(
        <div>
            <button onClick={clickHandler}>{children}</button>
        </div>
    )
}

export default React.memo(Button)
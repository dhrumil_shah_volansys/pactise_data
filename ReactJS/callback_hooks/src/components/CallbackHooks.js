import React, { useCallback, useState } from "react";
import Title from './Title'
import Count from './Count'
import Button from './Button'

const CallbackHooks = () => {

    const [age, setAge] = useState(10)
    const [salary, setSalary] = useState(50000)

    const incrementAge = useCallback(() => {
        setAge(age + 1)
    }, [age])

    const incrementSalary = useCallback(() => {
        setSalary(salary + 1000)
    }, [salary]) 

    return(
        <div>
            <Title callbackTitle="CallbackHooks"/>
            <Count text="Age" count={age}/>
            <Button clickHandler={incrementAge}>Increment Age</Button>
            <Count text="Salary" count={salary}/>
            <Button clickHandler={incrementSalary}>Increment Salary</Button>
        </div>
    )
}

export default CallbackHooks
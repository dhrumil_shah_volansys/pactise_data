import logo from './logo.svg';
import './App.css';
import CallbackHooks from './components/CallbackHooks';

function App() {
  return (
    <div className="App">
      <CallbackHooks/>
    </div>
  );
}

export default App;

import { useDispatch, useSelector } from "react-redux";
import {buyCake} from "../redux/cake/cakeAction";

const CakeContainer = () => {

    const numOfCakes = useSelector(state => state.cake.numberOfCakes)
    const dispatch = useDispatch()

    return(
        <div>
            <h2>Number of Remaining Cakes - {numOfCakes}</h2>
            <button onClick={() => dispatch(buyCake())}>Buy Cake</button>
        </div>
    )
}

export default CakeContainer
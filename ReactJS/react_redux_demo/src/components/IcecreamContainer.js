import { useDispatch, useSelector } from "react-redux"
import {buyIcecream} from '../redux/icecream/icecreamActions'

const IcecreamContainer = () => {

    const numOfIcecream = useSelector(state => state.icecream.numberOfIcecream)
    const dispatch = useDispatch()

    return(
        <div>
            <h2>Number of Remaining Icecream - {numOfIcecream}</h2>
            <button onClick={() => dispatch(buyIcecream())}>Buy Icecream</button>
        </div>
    )
}

export default IcecreamContainer
import BUY_ICECREAM from "./icecreamTypes";

const initialState = {
    numberOfIcecream: 20
}

const icecreamReducer = (state = initialState, action) => {
    console.log(`icecreamReducer Call ${JSON.stringify(state)}`)
    switch(action.type) {
        case BUY_ICECREAM:
            console.log(`state: ${JSON.stringify(state)}`) 
            return {
                ...state,
                numberOfIcecream: state.numberOfIcecream - 1
            }
        default: return state
    }
}

export default icecreamReducer
import { useParams } from "react-router-dom"

export const UserDetail = ({userId}) => {

    const params = useParams()  //return object of url params
    const id = params.userId

    return(
        <div>
            <h2>User Detail {id}</h2>
        </div>
    )
}
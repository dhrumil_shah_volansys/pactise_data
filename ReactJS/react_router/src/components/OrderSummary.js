import { useNavigate } from "react-router-dom"

export const OrderSummary = () => {

    const navigate = useNavigate()

    return(
        <>
            <div>
                Order is Confirmed!
            </div>
            <button onClick={() => navigate(-1)}>Back</button>
        </>
    )
}
import { Link, Outlet } from "react-router-dom"

export const Products = () => {
    return(
        <>
            <div>
                <input type='search' placeholder="Search Products"/>
            </div>
            <nav>
                <Link to='feature'>Feature</Link>
                <Link to='new'>New</Link>
            </nav>
            <Outlet/>
        </>
    )
}
import { Link, Outlet, useSearchParams } from "react-router-dom"

export const Users = () => {

    const [searchParams, setSearchParams] = useSearchParams()
    const params = searchParams.get('filter') === 'active'

    return(
        <>
            <div>
                <h2><Link to='1'>User 1</Link></h2>
                <h2><Link to='2'>User 2</Link></h2>
                <h2><Link to='3'>User 3</Link></h2>
            </div>
            <Outlet/>
            <div>
                <button onClick={() => setSearchParams({filter: 'active'})}>Active Users</button>
                <button onClick={() => setSearchParams({})}>Reset Users</button>
            </div>
            <div>
                <h1>
                    {params ? 'Active Users' : 'All users'}
                </h1>
            </div>
        </>
    )
}
import React from 'react';
import './App.css';
import {Route, Routes} from 'react-router-dom'
import { Home } from './components/Home';
// import { About } from './components/About';
import { Navbar } from './components/Navbar';
import { OrderSummary } from './components/OrderSummary';
import { Nomatch } from './components/NoMatch';
import { Products } from './components/Products';
import { Feature } from './components/Feature';
import { New } from './components/New';
import { Users } from './components/Users';
import { UserDetail } from './components/UserDetail';
import { Admin } from './components/Admin';
const LazyAbout = React.lazy(() => import('./components/About'))  //Lazy Loading

function App() {
  return (
    <>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='about' element={
          <React.Suspense fallback='Loading...'>
            <LazyAbout/>
          </React.Suspense>
        }/>
        <Route path='order_summary' element={<OrderSummary/>}/>
        <Route path='products' element={<Products/>}>
          <Route path='feature' element={<Feature/>}/>
          <Route path='new' element={<New/>}/>
        </Route>
        <Route path='users' element={<Users/>}>
          <Route path=':userId' element={<UserDetail/>}/>
          <Route path='admin' element={<Admin/>}/>
        </Route>
        <Route path='*' element={<Nomatch/>}/>
      </Routes>
    </>
  );
}

export default App;

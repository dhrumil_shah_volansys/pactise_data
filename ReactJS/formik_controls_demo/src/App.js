import logo from './logo.svg';
import './App.css';
import { FormikContainer } from './components/FormikContainer';
import { LoginForm } from './components/LoginForm';
import { RegistrationForm } from './components/RegistrationForm';
import { EnrollementForm } from './components/EnrollementForm';
import { Demo } from './components/Demo';

function App() {
  return (
    <div className="App">
      {/* <FormikContainer /> */}
      {/* <LoginForm /> */}
      {/* <RegistrationForm /> */}
      {/* <EnrollementForm /> */}
      <Demo />
    </div>
  );
}

export default App;

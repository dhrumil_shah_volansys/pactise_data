import { Form, Formik } from "formik"
import * as Yup from 'yup'
import { FormikControl } from "./FormikControl"

export const FormikContainer = () => {

    const dropdownOptions = [
        {key: 'Select Option', value: ''},
        {key: 'Option 1', value: 'option1'},
        {key: 'Option 2', value: 'option2'},
        {key: 'Option 3', value: 'option3'},
    ]
    const radioOptions = [
        {key: 'Option 1', value: 'roption1'},
        {key: 'Option 2', value: 'roption2'},
        {key: 'Option 3', value: 'roption3'},
    ]
    const checkBoxOptions = [
        {key: 'Option 1', value: 'coption1'},
        {key: 'Option 2', value: 'coption2'},
        {key: 'Option 3', value: 'coption3'},
    ]

    const initialValues = {
        email: '',
        description: '',
        selectOption: '',
        radioOption: '',
        checkBoxOption: [],
        birthDate: null
    }

    const validationSchema = Yup.object({
        email: Yup.string().email().required('Required'),
        description: Yup.string().required('Required!'),
        selectOption: Yup.string().required('Required!'),
        radioOption: Yup.string().required('Required!'),
        checkBoxOption: Yup.array().required('Required!'),
        birthDate: Yup.date().required('Required!').nullable()
    })

    const onSubmit = (values) => {console.log('Form Data => ', values)} 

    return(
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {
                (formik) => {
                    return(
                        <Form>
                            <FormikControl control='input' type='text' label='Email' name='email'/>
                            <FormikControl control='textarea' label='Description' name='description' />
                            <FormikControl control='select' label='Select Option' name='selectOption' options={dropdownOptions} />
                            <FormikControl control='radio' label='Radio Topic' name='radioOption' options={radioOptions} />
                            <FormikControl control='checkbox' label='Checkbox Topics' name='checkBoxOption' options={checkBoxOptions} />
                            <FormikControl control='date' label='Birth Date' name='birthDate' />
                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>
    )
}
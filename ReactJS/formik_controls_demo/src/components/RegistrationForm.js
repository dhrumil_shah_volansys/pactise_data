import { Form, Formik } from "formik"
import * as Yup from 'yup'
import { FormikControl } from './FormikControl'

export const RegistrationForm = () => {

    const contactOptions = [
        {key: 'Email', value: 'emailMOC'},
        {key: 'Phone', value: 'phoneMOC'}
    ]
    const initialValues = {
        email: '',
        password: '',
        confirmPassword: '',
        modeOfContact: '',
        phone: ''
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid Format').required('Required'),
        password: Yup.string().required('Required'),
        confirmPassword: Yup.string().oneOf([Yup.ref('password'), ''], 'Password not Match').required('Required'),
        modeOfContact: Yup.string().required('Required'),
        phone: Yup.string().when('modeOfContact', {
            is: 'phoneMOC',
            then: Yup.string().required('Required')
        })
    })
    const onSubmit = (values) => {
        console.log('Form Data => ', values)
    }

    return(
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {
                (formik) => {
                    return(
                        <Form>
                            <FormikControl control='input' type='text' label='Email' name='email' />
                            <FormikControl control='input' type='password' label='Password' name='password' />
                            <FormikControl control='input' type='password' label='Confirm Password' name='confirmPassword' />
                            <FormikControl control='radio' label='Mode of Contact' name='modeOfContact' options={contactOptions}/>
                            <FormikControl control='input' type='text' label='Phone' name='phone' />
                            <button type="submit" disabled={!formik.isValid}>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>
    )
}
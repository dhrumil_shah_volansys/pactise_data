import { Form, Formik } from "formik"
import * as Yup from 'yup'
import { FormikControl } from './FormikControl'

export const EnrollementForm = () => {

    const courseOptions = [
        {key: 'Select Course', value: ''},
        {key: 'React', value: 'react'},
        {key: 'Angular', value: 'angular'},
        {key: 'Vue', value: 'vue'}
    ]
    const skillOptions = [
        {key: 'Html', value: 'html'},
        {key: 'Css', value: 'css'},
        {key: 'Javascript', value: 'javascript'}
    ]
    const initialValues = {
        email: '',
        bio: '',
        course: '',
        skills: [],
        courseDate: null
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid Format').required('Required'),
        bio: Yup.string().required('Required'),
        course: Yup.string().required('Required'),
        skills: Yup.array().required('Required'),
        courseDate: Yup.date().required('Required').nullable()
    })
    const onSubmit = values => {
        console.log('Form Data => ', values)
    }

    return(
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {
                (formik) => {
                    return(
                        <Form>
                            <FormikControl control='input' label='Email' name='email' type='email' />
                            <FormikControl control='textarea' label='Bio' name='bio' />
                            <FormikControl control='select' label='Courses' name='course' options={courseOptions} />
                            <FormikControl control='checkbox' label='Skill' name='skills' options={skillOptions} />
                            <FormikControl control='date' label='Course Date' name='courseDate'/>
                            <button type="submit" disabled={!formik.isValid}>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>
    )
}
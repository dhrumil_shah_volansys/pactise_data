import { CheckBoxOptions } from "./CheckBoxOptions"
import { DatePicker } from "./DatePicker"
import { Input } from "./Input"
import { RadioOptions } from "./RadioOptions"
import { Select } from "./Select"
import { Textarea } from "./Textarea"

export const FormikControl = (props) => {

    const {control, ...rest} = props

    switch(control){
        case 'input':
            return <Input {...rest} />
        case 'textarea':
            return <Textarea {...rest} />
        case 'select':
            return <Select {...rest} />
        case 'radio':
            return <RadioOptions {...rest} />
        case 'checkbox':
            return <CheckBoxOptions {...rest} />
        case 'date':
            return <DatePicker {...rest} />
        default: return null
    }
}
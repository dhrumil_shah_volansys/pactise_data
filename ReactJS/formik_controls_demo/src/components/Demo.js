import React, { useEffect, useState } from 'react'

export const Demo = () => {

    const [update, setUpdate] = useState({fname: '', lname: ''})

    const setData = (event) => {
        console.log('Data is set', event)
        setUpdate({fname: 'Dhrumil', lname: 'Shah'})
    }

    useEffect(() => {
        console.log('useEffect called')
        window.addEventListener('mousemove', setData)
    }, [])

    return(
        <div>
            <h1>Data: {update.fname} and {update.lname}</h1>
        </div>
    )
}
import { Formik, Form, Field, ErrorMessage, FieldArray, FastField } from "formik"
import { useState } from "react"
import * as Yup from 'yup'
import { Error } from "./Error"

// const initialValues = {
//     name: '',
//     email: '',
//     channel: '',
//     comments: '',
//     address: '',
//     social: {
//         facebook: '',
//         twitter: ''
//     },
//     phoneNumbers: ['', ''],
//     phNumbers: ['']
// }
const savedValues = {
    name: 'Dhrumil',
    email: 'dj@gmail.com',
    channel: 'djvolution',
    comments: 'Welcome to channel',
    address: '123 anand',
    social: {
        facebook: '',
        twitter: ''
    },
    phoneNumbers: ['', ''],
    phNumbers: ['']
}

const onSubmit = (values) => {
    console.log(`Form Data => ${JSON.stringify(values)}`)
}

const validationSchema = Yup.object({
    name: Yup.string().required('Required!'),
    email: Yup.string().email('Invalid Format').required('Required!'),
    channel: Yup.string().required('Required!')
})

const validateComments = (values) => {
    let error = {}
    if(!values){
        error = 'Required!'
    }
    return error
}

export const YoutubeForm = () => {
    const [formValues, setFormValues] = useState(null)
    return (
        <Formik 
            initialValues={savedValues} 
            onSubmit={onSubmit} 
            validationSchema={validationSchema}
            // validateOnChange={false}
            // validateOnBlur={false}
            // validateComments={validateComments}
            enableReinitialize
        >
            {
                formik => {
                    console.log('Formik => ', formik)
                    return(
                        <Form>
                            <div className="form-control">
                                <label htmlFor='name'>Name</label>
                                <Field type='text' id='name' name='name' />
                                <ErrorMessage name="name" component={Error}/>
                            </div>
                            <div className="form-control">
                                <label htmlFor='email'>Email</label>
                                <Field type='email' id='email' name='email' />
                                {/* <ErrorMessage name="email">
                                    {
                                        (errorMsg) => {
                                            <div className="error">{errorMsg}</div>
                                        }
                                    }
                                </ErrorMessage> */}
                                <ErrorMessage name="email" component={Error}/>
                            </div>
                            <div className="form-control">
                                <label htmlFor='channel'>Channel</label>
                                <Field type='text' id='channel' name='channel' />
                                <ErrorMessage name="channel" component={Error}/>
                            </div>
                            <div className="form-control">
                                <label htmlFor='comments'>Comments</label>
                                <Field as='textarea' id='comments' name='comments' validate={validateComments} />
                                <ErrorMessage name="comments" component={Error}/>
                            </div>
                            <div className="form-control">
                                <label htmlFor='address'>Address</label>
                                <FastField name='address'>
                                    {
                                        (props) => {
                                            const {field, form, meta} = props
                                            return(
                                                <div>
                                                    <input type='text' id="address" {...field}/>
                                                    {meta.touched && meta.error ? <div>{meta.error}</div> : null}
                                                </div>
                                            )
                                        }
                                    }
                                </FastField>
                            </div>
                            <div className="form-control">
                                <label htmlFor='facebook'>Facebook Profile</label>
                                <Field type='text' id='facebook' name='social.facebook' />
                            </div>
                            <div className="form-control">
                                <label htmlFor='twitter'>Twitter Profile</label>
                                <Field type='text' id='twitter' name='social.twitter' />
                            </div>
                            <div className="form-control">
                                <label htmlFor='primaryPh'>Primary Phone Number</label>
                                <Field type='text' id='primaryPh' name='phoneNumbers[0]' />
                            </div>
                            <div className="form-control">
                                <label htmlFor='secondaryPh'>Secondary Phone Number</label>
                                <Field type='text' id='secondaryPh' name='phoneNumbers[1]' />
                            </div>
                            <div className="form-control">
                                <label htmlFor='phoneList'>Phone Number List</label>
                                <FieldArray name="phNumbers">
                                    {
                                        (fieldArrays) => {
                                            const {push, remove, form} = fieldArrays
                                            const {values} = form
                                            const {phNumbers} = values
                                            return(
                                                <div>
                                                    {
                                                        phNumbers.map((number, index) => (
                                                            <div key={index}>
                                                                <Field name={`phNumbers[${index}]`}/>
                                                                {
                                                                    index > 0 && <button onClick={() => remove(index)}>-</button>
                                                                }
                                                                <button onClick={() => push('')}>+</button>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            )
                                        }
                                    }
                                </FieldArray>
                            </div>
                            {/* <button type="button" onClick={() => formik.validateField('comments')}>Validate Comments</button>
                            <button type="button" onClick={() => formik.validateForm()}>Validate Form</button>
                            <button type="button" onClick={() => formik.setFieldTouched('comments')}>Visit Comments</button>
                            <button type="button" onClick={() => formik.setTouched({
                                name: true,
                                email: true,
                                channel: true,
                                comments: true
                            })}>Visit Fields</button> */}
                            <button type="button" onClick={() => setFormValues(savedValues)}>Saved data</button>
                            <button type='submit' disabled={!formik.isValid || formik.isSubmitting}>Submit</button>
                        </Form>
                    )
                }    
            }
        </Formik>
    )
}
import styled, { keyframes } from "styled-components";

export const StyledButton = styled.button`
    background-color: ${(props) => props.variant === 'outline' ? '#FFF' : '#7CFC00'};
    color: ${(props) => props.variant === 'outline' ? '#7CFC00' : '#FFF'};
    border: 2px solid #7CFC00;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    font-size: 16px;
    &:hover {
        background-color: ${(props) => props.variant !== 'outline' ? '#FFF' : '#7CFC00'};
        color: ${(props) => props.variant !== 'outline' ? '#7CFC00' : '#FFF'};
    }
`
export const FansyButton = styled(StyledButton)`
    background-image: linear-gradient(to right, #0000FF, #FFFF00, #FF0000)
`
export const SubmitButton = styled(StyledButton).attrs((props) => ({
    type: 'submit',
}))`
    box-shadow: 0 9px $999;
`
const rotate = keyframes`
    from{
        transform: rotate(0deg);
    }
    to{
        transform: rotate(360deg);
    }
`
export const AnimatedLogo = styled.img`
    height: 40vmin;
    pointer-events: none;
    animation: ${rotate} infinite 20s linear;
`
export const DarkButton = styled(StyledButton)`
    border: 2px solid ${(props) => props.theme.dark.primary};
    background-color: ${(props) => props.theme.dark.primary};
    color: ${(props) => props.theme.dark.text};

`


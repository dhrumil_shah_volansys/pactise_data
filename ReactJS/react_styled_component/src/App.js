import logo from './logo.svg';
import './App.css';
import StyledButton from './components/Button/Button';
import {FansyButton, SubmitButton} from './components/Button/Button';
import { AnimatedLogo, DarkButton } from './components/Button/ButtonStyled';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

const theme = {
  dark:{
    primary: '#000',
    text: '#fff'
  },
  light:{
    primary: '#fff',
    text: '#000'
  },
  fontFamily: 'Segoe UI' 
}

const GlobalStyle = createGlobalStyle`
  button {
    font-family: ${(props) => props.theme.fontFamily}
  }
`

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle/>
      <div className="App">
        <AnimatedLogo src={logo}/>
        <div>
          <br></br>
        </div>
        <StyledButton>Styled button</StyledButton>
        <div>
          <br></br>
        </div>
        <StyledButton variant='outline'>Styled button</StyledButton>
        <div>
          <br></br>
        </div>
        <FansyButton>Fansy Button</FansyButton>
        <div>
          <br></br>
        </div>
        <SubmitButton>Submit Button</SubmitButton>
        <div>
          <br></br>
        </div>
        <DarkButton>Dark Button</DarkButton>
      </div>
    </ThemeProvider>
  );
}

export default App;

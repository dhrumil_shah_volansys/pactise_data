import logo from './logo.svg';
import './App.css';
import Expense from './components/Expense/Expense';

const App = () => {

  const expense = [
    {
      id: 'e1',
      title: 'car Insurance',
      price: '200',
      date: new Date(2022, 1, 14)
    },
    {
      id: 'e2',
      title: 'bike Insurance',
      price: '300',
      date: new Date(2022, 1, 15)
    },
    {
      id: 'e3',
      title: 'health Insurance',
      price: '400',
      date: new Date(2022, 1, 16)
    },
    {
      id: 'e4',
      title: 'family Insurance',
      price: '500',
      date: new Date(2022, 1, 17)
    }
  ]

  return (
    <div className="App">
      <Expense items={expense}/>
    </div>
  );
}

export default App;

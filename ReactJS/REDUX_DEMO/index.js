const redux = require('redux')
const reduxLogger = require('redux-logger')
const logger = reduxLogger.createLogger()
const applyMiddleware = redux.applyMiddleware

// Initial content 
const initialCakeState = {
    numberOfCake: 10
}
const initialIcecreamState = {
    numberOfIcecream: 20
}

// Action functions
const buyCake = () => {
    return {
        type: 'buy_cake'
    }
}
const buyIcecream = () => {
    return {
        type: 'buy_icecream'
    }
}

// create reducer as shopkeeper 
const cakeShopkeeperreducer = (state = initialCakeState, action) => {
    switch(action.type){
        case 'buy_cake':
            return {
                ...state,
                numberOfCake: state.numberOfCake - 1
            }
        default: return state
    }
}
const icecreamShopkeeperreducer = (state = initialIcecreamState, action) => {
    switch(action.type){
        case 'buy_icecream':
            return {
                ...state,
                numberOfIcecream: state.numberOfIcecream - 1
            }
        default: return state
    }
}

// Combined all the reducers OR shopkeepers
const combineReducer = redux.combineReducers    
const allReducers = combineReducer({
    cakereducer: cakeShopkeeperreducer,
    icecreamreducer: icecreamShopkeeperreducer
})

// Create Store 
const createStore = redux.createStore
const store = createStore(allReducers, applyMiddleware(logger))
console.log(`initial state ${JSON.stringify(store.getState())}`)

// Update the state 
const unsubscribe = store.subscribe(() => {})

// customer dispatch cake buying request to shopkeeper 
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIcecream())
store.dispatch(buyIcecream())
unsubscribe()
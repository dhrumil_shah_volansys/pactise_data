from crypt import methods
from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def student_info():
    return render_template('student_info.html')

@app.route('/info', methods=['POST', 'GET'])
def info_table():
    print("=====REQUEST=======", request)
    if request.method == 'POST':
        print("====REQUEST.FORM=====", request.form)
        result = request.form
        return render_template('info_table.html', result=result)

if __name__ == '__main__':
    app.run(debug=True)
from os import abort
from flask import Flask, render_template, request, session, redirect, url_for, abort

app = Flask(__name__)
app.secret_key = "dhrumil"  

@app.route('/error')
def error():
    return '<h2>Invalid Password</h2>'

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/success', methods=['POST', 'GET'])
def success():
    if request.method == 'POST':
        uname = request.form['uname']
        pswd = request.form['pwd']
        if pswd == 'abc':
            session['username'] = uname
            return render_template('success.html')
        else:
            abort(401)
    else:
        return redirect(url_for('login'))

@app.route('/profile')
def profile():
    if 'username' in session:
        uname = session['username']
        return render_template('profile.html', uname=uname)
    else:
        return '<h2>Please Login First</h2>'

@app.route('/logout')
def logout():
    if 'username' in session:
        session.pop('username', None)
        return render_template('logout.html')
    else:
        return '<h2>You already Logout</h2>'

if __name__ == '__main__':
    app.run(debug=True)
from flask_wtf import Form
from wtforms import StringField, IntegerField, SelectField, RadioField, TextAreaField, SubmitField, EmailField
from wtforms import validators, ValidationError
from wtforms.validators import DataRequired, Email

class ContactForm(Form):
    name = StringField('Name', [DataRequired('Please Enter Your Name')])
    email = EmailField('Email', [DataRequired('Please Enter Your Email')])
    gender = RadioField('Gender', choices = [('M', 'Male'), ('F', 'Female')])
    address = TextAreaField('Address')
    age = IntegerField('Age')
    language = SelectField('Language', choices=[('cpp', 'C++'), ('py', 'Python')])
    submit = SubmitField('Submit')
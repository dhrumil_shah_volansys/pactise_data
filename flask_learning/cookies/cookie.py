from flask import Flask, render_template, request, make_response, redirect, url_for

app = Flask(__name__)

@app.route('/')
def login():
    return render_template('login.html')

@app.route('/error')
def error():
    return '<h1>Please enter correct Password</h1>'

@app.route('/success', methods=['POST', 'GET'])
def success():
    if request.method == 'POST':
        user = request.form['uname']
        pawd = request.form['pwd']

    if pawd == 'abc':
        resp = make_response(render_template('success.html'))
        resp.set_cookie('username',user)
        return resp
    else:
        return redirect(url_for('error'))

@app.route('/profile')
def profile():
    uname = request.cookies.get('username')
    resp = make_response(render_template('profile.html', uname=uname))
    return resp

if __name__ == '__main__':
    app.run(debug=True)
from flask import Flask, request, redirect, url_for

app = Flask(__name__)

@app.route('/welcome/<name>')
def welcome(name):
    return 'Welcome %s' % name

@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        uname = request.form['nm']
        return redirect(url_for('welcome', name=uname))
    else:
        uname = request.args.get('nm')
        return redirect(url_for('welcome', name=uname))

if __name__ == '__main__':
    app.run(debug=True)
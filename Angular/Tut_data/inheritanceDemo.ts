class Person{
    constructor(private fname: string, private lname: string){
        this.fname = fname
        this.lname = lname
    }

    getFullName(): string{
        return `${this.fname} ${this.lname}`
    }

    describe(): string{
        return `This is ${this.fname} ${this.lname}`
    }
}

class Employee extends Person{
    constructor(fname: string, lname: string, private pid: number){
        super(fname, lname)
    }
}

let emp = new Employee('Dhrumil', 'Shah', 3)
console.log(emp.getFullName())
console.log(emp.describe())
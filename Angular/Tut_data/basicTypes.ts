let age: number = 10
let fullname: string = 'typescript'
let phnumber: bigint = 100n
let list: number[] = [1, 2, 3]

// function parameter
function getfavnumber(number: number) {
    console.log(number)
} 
getfavnumber(5)
function objectType(pr: {x: string, y: number}){
    console.log(`Hii ${pr.x}${pr.y}`)
}
objectType({x: 'Dhrumil', y: 4})
function anyParameter(inputData: string | number | boolean){
    if (typeof(inputData) === 'string'){
        console.log(`Input data type of ${inputData.toUpperCase()} is ${typeof(inputData)}`)
    }else{
        console.log(`Input data type of ${inputData} is ${typeof(inputData)}`)
    }
}
anyParameter("name")
function arrayInput(inputArray: string[] | string){
    if (Array.isArray(inputArray)){
        console.log(`Array is ${inputArray.join(' and ')}`)
    }else{
        console.log(`string is ${inputArray}`)
    }
}
// arrayInput(['name', 'surname', 'mouse'])
arrayInput('Djashaj')

// function return
function returnString(): string{
    return "hello"
}
console.log(returnString()) 

// interface 
interface Point {
    x: string,
    y: number
}
function useInterface(pt: Point){
    console.log(`${pt.x} and ${pt.y}`)
}
useInterface({x: 'Djahah', y: 5})
// classes 
// class Person{
//     fname: string
//     lname: string
//     rno: number

//     constructor(fname: string, lname: string, rno: number){
//         this.fname = fname
//         this.lname = lname
//         this.rno = rno
//     }

//     getPersonData(): string{
//         return `${this.fname} and ${this.lname} and ${this.rno}`
//     }
// }

// let person = new Person("Dhrumil", "Shah", 5)
// console.log(person.getPersonData())

// Access modifiers
// class Person{
//     private fname: string
//     private lname: string
//     rno: number

//     constructor(fname: string, lname: string, rno: number){
//         this.fname = fname
//         this.lname = lname
//         this.rno = rno
//     }

//     getPersonData(): string{
//         return `${this.fname} and ${this.lname} and ${this.rno}`
//     }
// }

// let person = new Person("Dhrumil", "Shah", 5)
// person.getPersonData()
// console.log(person.rno)
console.log('Hello world !!!!!')

let str_var = 'dhrumil'
let int_var = 4
let bool_var = true
let null_var = null
let undefined_var
// val = `${typeof(str_var)}: ${str_var}, ${typeof(int_var)}: ${int_var}, ${typeof(bool_var)}: ${bool_var}, ${typeof(null_var)}: ${null_var}, ${typeof(undefined_var)}: ${undefined_var}`

const const_var = 56
// val = const_var

let object_var = {
    name: 'djshah',
    age: 6
}
object_var.age = 7
// val = object_var

let arr = ['ramik', 5, false, null]
// val = arr.length
// arr.push(3593)
// arr.pop()
// arr.unshift('shah')
// arr.shift('jatin')
// arr.splice(1, 2, 'dhrumil', 'shah')
// arr.reverse()
// val = arr

function square(number){
    return number * number
}
// val = square(3)

const d = new Date()
// val = d.getDate()
// val = d.getMonth()
// val = d.getFullYear()
// val = d.getHours()
// val = d.getMinutes()
// val = d.getMilliseconds()
// val = d.setDate(25)
// val = d.setMonth(3)

let comp_num = '15'
// if (comp_num >= 18){
//     val = 'ready for vote'
// }else if (comp_num>10 && comp_num < 18){
//     val = 'you are mature'
// }else if(comp_num === 15){
//     val = 'compare with datatype'
// }else{
//     val = 'You are kid'
// }

let press_button = 2
// switch(press_button){
//     case 1:
//         val = 'Star Plus'
//         break
//     case 2:
//         val = 'Sony'
//         break
//     case 3:
//         val = 'Colors'
//         break
//     default:
//         val = 'Invalid press'
//         break        
// }

let person = {
    square: function(number){
        return number * number
    },
    cube: function(number){
        return number * number * number
    }
}
// val = person.square(3)
// val = person.cube(3)

let names = ['D', 'h', 'r', 'u', 'm', 'i', 'l']
// for(let element in names){
//     console.log(names[element])
// }

console.log(val)
console.log("Hello World !!!!")

// let doc
// doc = document.getElementById('dom')
// document.getElementById('dom').innerHTML = 'Dhrumil Shah'

// doc = document.getElementsByTagName("p")
// val = doc
// document.getElementById('dom').innerHTML = 'select: ' + document.getElementsByTagName('p')[0].innerHTML

// doc = document.getElementsByClassName('intro')
// val = doc
// document.getElementById('dom').innerHTML = 'select: ' + document.getElementsByClassName('intro')[1].innerHTML

// console.log(val)

// console.log(document.head)
// console.log(document.body)
// console.log(document.body.childNodes)
// console.log(document.body.children)
// console.log(document.body.children[0].children)
// console.log(document.body.firstElementChild)
// console.log(document.body.firstElementChild.nextElementSibling)
// console.log(document.body.children[0].children[1].previousElementSibling.textContent)
// console.log(document.body.children[0].children[1].nextElementSibling.textContent)

// Exercise
// console.log(document.body.children[1].children[0].children)
// let arr = ['red', 'black', 'blue', 'yellow', 'green']
// for (let i in arr){
//     document.body.children[1].children[0].children[i].style = `background-color: ${arr[i]};`
// }

// element selector
// console.log(document.getElementById('element'))
// document.getElementById('element').style.backgroundColor = 'brown'
// console.log(document.getElementsByClassName('list-item'))

// query selector
// const query_selector = document.querySelector(".list-item")
// const query_selector_all = document.querySelectorAll(".list-item")
// console.log(query_selector)
// console.log(query_selector_all)

// attribute
// console.log(element.getAttribute('data'))
// element.setAttribute('status', 'done')
// console.log(element.getAttribute('status'))
// console.log(element.hasAttribute('status'))
// element.removeAttribute('status')
// console.log(element.hasAttribute('status'))

// exercise
// let selector = 'a[href*="://"]:not([href^="http://internal.com"])';
// let links = document.querySelectorAll(selector);
// links.forEach(link => link.style.color = 'orange');

// creating and removing element
// let newDiv = document.createElement('div')
// newDiv.innerHTML = `
//     <h1>Created new div</h1>
//     <p>This are the html inside div tag</p>
// `
// body.append(newDiv)
// body.prepend(newDiv)
// body.before(newDiv)
// body.after(newDiv)
// link_set.replaceWith(newDiv)
// link_set.remove()

// let entered_name = window.prompt("Enter Your Name: ")
// let namedisplayDiv = document.createElement("h1")
// namedisplayDiv.innerHTML = `Your name is: ${entered_name}`
// body.append(namedisplayDiv)

function btnevent(){
    console.log(this)
    alert("Hello")
}

// myBtn.onclick = btnevent
myBtn.addEventListener('click', btnevent)
from django.shortcuts import render, redirect
from .forms import VideoUploadForm

# Create your views here.
def home(request):
    return render(request, 'uploderapp/home.html')

def index(request):
    if request.method == 'POST':
        form = VideoUploadForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = VideoUploadForm()
    return render(request, 'uploderapp/index.html', {
        'form': form
    })